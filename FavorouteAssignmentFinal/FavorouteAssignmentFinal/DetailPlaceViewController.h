//
//  DetailPlaceViewController.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import "Place.h"
#import "GoogleMapView.h"
#import "AppleMapView.h"
#import "MapBoxMapView.h"

@interface DetailPlaceViewController : UIViewController <GMSMapViewDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *website;
@property (weak, nonatomic) IBOutlet UILabel *latitude;
@property (weak, nonatomic) IBOutlet UILabel *longitude;

@property (strong, nonatomic) Place *place;

@property (strong, nonatomic) GoogleMapView *googleMapView;
@property (strong, nonatomic) AppleMapView *appleMapView;
@property (strong, nonatomic) MapBoxMapView *oMSMapView;
@property (strong, nonatomic) NSMutableArray *maps;
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property NSUInteger currentMap;

- (IBAction)chageMap:(UISegmentedControl *)sender;
- (void)reloadData;

@end
