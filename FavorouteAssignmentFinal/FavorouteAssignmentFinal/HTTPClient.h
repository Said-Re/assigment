//
//  HTTPClient.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 03/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "Constants.h"
#import "Place.h"

@interface HTTPClient : AFHTTPSessionManager

+ (instancetype)sharedClient;
- (void)getPlace:(NSString *)text setPlace:(void (^)(id placesForSend))sendData;
- (void)getDetails:(Place *)place setDetails:(void (^)(id detailsForSend))sendData;

@end
