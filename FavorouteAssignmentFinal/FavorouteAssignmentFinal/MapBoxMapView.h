//
//  MapBoxMapView.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mapbox.h"

@interface MapBoxMapView : UIView

@property (strong,nonatomic) RMMapView *mapView;

- (id) initWithFrame:(CGRect)aRect;
- (void)setMark:(CLLocationCoordinate2D)position;


@end
