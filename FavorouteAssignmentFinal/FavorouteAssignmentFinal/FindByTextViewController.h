//
//  FindByTextViewController.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 04/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPClient.h"
#import "Place.h"
#import "PlacesTableViewController.h"

@interface FindByTextViewController : UIViewController <UITextFieldDelegate>

@end
