//
//  MapQuestMapView.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MapQuestMapView : UIView

@property (strong,nonatomic) MQMapView *mapView;

- (id) initWithFrame:(CGRect)aRect;
//- (void)setDelegate:(id <GMSMapViewDelegate>)receiver;
- (void)setMark:(CLLocationCoordinate2D)position;

@end



