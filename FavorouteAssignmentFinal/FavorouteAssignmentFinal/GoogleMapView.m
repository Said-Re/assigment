//
//  GoogleMapView.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "GoogleMapView.h"

@implementation GoogleMapView

- (void)setup
{
    self.mapView = [[GMSMapView alloc] initWithFrame:self.frame];
    [self addSubview:self.mapView];
}

- (void)awakeFromNib { [self setup]; }

- (id) initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    [self setup];
    return self;
}

- (void)setDelegate:(id <GMSMapViewDelegate>)receiver
{
    self.mapView.delegate = receiver;
}

- (void)setMark:(CLLocationCoordinate2D)position
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:position zoom:15];
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    marker.icon = [GMSMarker markerImageWithColor:color];
    marker.map = self.mapView;
    [self.mapView setCamera:camera];
}

@end
