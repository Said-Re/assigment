//
//  FindByCoordinatesViewController.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleMapView.h"
#import "AppleMapView.h"
#import "MapBoxMapView.h"

@interface FindByCoordinatesViewController : UIViewController <UITextFieldDelegate, GMSMapViewDelegate, MKMapViewDelegate, RMMapViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (strong, nonatomic) GoogleMapView *googleMapView;
@property (strong, nonatomic) AppleMapView *appleMapView;
@property (strong, nonatomic) MapBoxMapView *oMSMapView;
@property (strong, nonatomic) NSMutableArray *maps;

@property (weak, nonatomic) IBOutlet UITextField *latitude;
@property (weak, nonatomic) IBOutlet UITextField *longitude;

@property float textfieldY;
@property NSUInteger currentMap;

- (IBAction)changeMap:(UISegmentedControl *)sender;

- (IBAction)find:(UIButton *)sender;

@end
