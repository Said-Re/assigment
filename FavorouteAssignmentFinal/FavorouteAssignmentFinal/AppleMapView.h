//
//  AppleMapView.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface AppleMapView : UIView

@property (nonatomic, strong) MKMapView *mapView;

- (id) initWithFrame:(CGRect)aRect;
- (void)setDelegate:(id <MKMapViewDelegate>)receiver;
- (void)setMark:(CLLocationCoordinate2D)position;

@end
