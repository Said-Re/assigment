//
//  Constants.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 03/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

extern NSString* const googlePlaceAPIBaseURLString;
extern NSString* const googlePlaceAPIKEY;
extern NSString* const googleMapsAPIKEY;

