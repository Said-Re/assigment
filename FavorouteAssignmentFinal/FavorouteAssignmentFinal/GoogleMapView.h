//
//  GoogleMapView.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapView : UIView

@property (strong,nonatomic) GMSMapView *mapView;

- (id) initWithFrame:(CGRect)aRect;
- (void)setDelegate:(id <GMSMapViewDelegate>)receiver;
- (void)setMark:(CLLocationCoordinate2D)position;

@end
