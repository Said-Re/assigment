//
//  Place.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 03/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "Place.h"

@implementation Place

- (void)setData:(NSDictionary *)json
{
    self.address = [json objectForKey:@"formatted_address"];
    self.name = [json objectForKey:@"name"];
    self.reference = [json objectForKey:@"reference"];
    
    NSDictionary *geometry = [json objectForKey:@"geometry"];
    NSDictionary *location = [geometry objectForKey:@"location"];
    float latitude = [[location objectForKey:@"lat"] floatValue];
    float longitude = [[location objectForKey:@"lng"] floatValue];
    
    self.location = CLLocationCoordinate2DMake(latitude, longitude);
}

- (void)setDetails:(NSDictionary *)json
{
    self.phone = [json objectForKey:@"international_phone_number"];
    self.webSite = [json objectForKey:@"website"];
    
}

@end
