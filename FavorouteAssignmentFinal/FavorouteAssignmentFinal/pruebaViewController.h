//
//  pruebaViewController.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapView.h"

@interface pruebaViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *map;
@property (strong, nonatomic) GoogleMapView *mapview;
@end
