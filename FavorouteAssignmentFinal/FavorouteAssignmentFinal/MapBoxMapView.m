//
//  MapBoxMapView.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "MapBoxMapView.h"

@implementation MapBoxMapView


- (void)setup
{
    self.mapView = [[RMMapView alloc] initWithFrame:self.frame];
    
    
    RMMapboxSource *onlineSource = [[RMMapboxSource alloc] initWithMapID:@"examples.map-z2effxa8"];
    RMMapView *mapView = [[RMMapView alloc] initWithFrame:self.bounds andTilesource:onlineSource];
    
    [self.mapView setZoom:2.0];
    [self addSubview:self.mapView];
     /*
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        
        RMMapboxSource *onlineSource = [[RMMapboxSource alloc] initWithMapID:@"examples.map-z2effxa8"];
        RMMapView *mapView = [[RMMapView alloc] initWithFrame:self.bounds andTilesource:onlineSource];
        
        [self.mapView setZoom:2.0];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self addSubview:self.mapView];
        });
    });
      */
}

- (void)awakeFromNib { [self setup]; }

- (id) initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    [self setup];
    return self;
}

- (void)setMark:(CLLocationCoordinate2D)position
{
    RMPointAnnotation *annotation = [[RMPointAnnotation alloc] initWithMapView:self.mapView
                                                                 coordinate:position andTitle:@"marca"];
    [self.mapView addAnnotation:annotation];
    [self.mapView setZoom:15.0 atCoordinate:position animated:YES];
}

@end
