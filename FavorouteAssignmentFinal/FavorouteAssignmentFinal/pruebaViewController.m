//
//  pruebaViewController.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "pruebaViewController.h"

@interface pruebaViewController ()

@end

@implementation pruebaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapview = [[GoogleMapView alloc] initWithFrame:self.map.frame];
    [self.map addSubview:self.mapview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
