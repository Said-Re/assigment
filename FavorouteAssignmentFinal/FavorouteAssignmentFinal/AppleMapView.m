//
//  AppleMapView.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "AppleMapView.h"

@implementation AppleMapView

- (void)setup
{
    self.mapView = [[MKMapView alloc] initWithFrame:self.frame];
    [self addSubview:self.mapView];
}

- (void)awakeFromNib { [self setup]; }

- (id) initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    [self setup];
    return self;
}

- (void)setDelegate:(id <MKMapViewDelegate>)receiver
{
    self.mapView.delegate = receiver;
}

- (void)setMark:(CLLocationCoordinate2D)position
{
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(position, 2000, 2000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    int x =  arc4random() % 5;
    x++;
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    [point setCoordinate:position];
    [point setAccessibilityLabel:[NSString stringWithFormat:@"%i", x]];
    
    [self.mapView addAnnotation:point];
}


@end
