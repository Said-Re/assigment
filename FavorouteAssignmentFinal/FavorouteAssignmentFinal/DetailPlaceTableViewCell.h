//
//  DetailPlaceTableViewCell.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 04/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailPlaceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
