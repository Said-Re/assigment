//
//  DetailPlaceViewController.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "DetailPlaceViewController.h"

@interface DetailPlaceViewController ()

@end

@implementation DetailPlaceViewController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.name.text = self.place.name;
    self.address.text = self.place.address;
    self.latitude.text = [NSString stringWithFormat:@"%f",self.place.location.latitude];
    self.longitude.text = [NSString stringWithFormat:@"%f",self.place.location.longitude];
    self.phone.text = self.place.phone;
    self.website.text = self.place.webSite;
    
    //Google Map
    self.googleMapView = [[GoogleMapView alloc] initWithFrame:self.mapView.frame];
    [self.googleMapView setDelegate:self];
    
    //Apple Map
    self.appleMapView = [[AppleMapView alloc] initWithFrame:self.mapView.frame];
    [self.appleMapView setDelegate:self];
    
    //Open Street Map
    self.oMSMapView = [[MapBoxMapView alloc] initWithFrame:self.mapView.frame];
    
    [self.googleMapView setMark:self.place.location];
    [self.appleMapView setMark:self.place.location];
    [self.oMSMapView setMark:self.place.location];

    [self.mapView addSubview:self.googleMapView];
    self.currentMap = 0;
    
    self.maps = [[NSMutableArray alloc] initWithObjects:self.googleMapView, self.appleMapView, self.oMSMapView,nil];
    
}

- (void)reloadData
{
    self.name.text = self.place.name;
    self.address.text = self.place.address;
    self.latitude.text = [NSString stringWithFormat:@"%f",self.place.location.latitude];
    self.longitude.text = [NSString stringWithFormat:@"%f",self.place.location.longitude];
    self.phone.text = self.place.phone;
    self.website.text = self.place.webSite;
    
    [self.googleMapView setMark:self.place.location];
    [self.appleMapView setMark:self.place.location];
    [self.oMSMapView setMark:self.place.location];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    marker.map = nil;
    
    return YES;
}

- (IBAction)chageMap:(UISegmentedControl *)sender
{
    [[self.maps objectAtIndex:self.currentMap] removeFromSuperview];
    [self.mapView addSubview:[self.maps objectAtIndex:sender.selectedSegmentIndex]];
    self.currentMap = sender.selectedSegmentIndex;
}
@end
