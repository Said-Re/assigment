//
//  FindByCoordinatesViewController.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 05/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "FindByCoordinatesViewController.h"

@interface FindByCoordinatesViewController ()

@end

@implementation FindByCoordinatesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Google Map
    self.googleMapView = [[GoogleMapView alloc] initWithFrame:self.mapView.frame];
    [self.googleMapView setDelegate:self];
    
    //Apple Map
    self.appleMapView = [[AppleMapView alloc] initWithFrame:self.mapView.frame];
    [self.appleMapView setDelegate:self];
    
    //Open Street Map
    self.oMSMapView = [[MapBoxMapView alloc] initWithFrame:self.mapView.frame];
    
    [self.mapView addSubview:self.googleMapView];
    self.currentMap = 0;
    
    self.maps = [[NSMutableArray alloc] initWithObjects:self.googleMapView, self.appleMapView, self.oMSMapView,nil];
    
    self.latitude.delegate = self;
    self.longitude.delegate = self;
    
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    marker.map = nil;
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textfieldY = textField.frame.origin.y + textField.frame.size.height;
    textField.placeholder = @"";
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.latitude] && [self.latitude.text isEqual:@""] )
        [self initLatitude];
    
    else if ([textField isEqual:self.longitude] && [self.longitude.text isEqual:@""] )
        [self initLongitude];
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Keyboard events

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyBoardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGRect viewWithKeyBoard = CGRectMake(0, keyBoardSize.height, self.view.bounds.size.width, self.view.bounds.size.height);
    [self.view setBounds:viewWithKeyBoard];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    CGRect viewWithKeyBoard = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    [self.view setBounds:viewWithKeyBoard];
}

- (void)initLatitude
{
    self.latitude.text = @"";
    self.latitude.placeholder = @"latitude..";
}

- (void)initLongitude
{
    self.longitude.text = @"";
    self.longitude.placeholder = @"longitude..";
}

- (IBAction)changeMap:(UISegmentedControl *)sender
{
    [[self.maps objectAtIndex:self.currentMap] removeFromSuperview];
    [self.mapView addSubview:[self.maps objectAtIndex:sender.selectedSegmentIndex]];
    self.currentMap = sender.selectedSegmentIndex;
}

- (IBAction)find:(UIButton *)sender
{
    float newLatitude = [self.latitude.text floatValue];
    float newLongitud = [self.longitude.text floatValue];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLatitude,newLongitud);
    
    [self.googleMapView setMark:position];
    [self.appleMapView setMark:position];
    [self.oMSMapView setMark:position];
    
    [self initLatitude];
    [self initLongitude];
}

@end
