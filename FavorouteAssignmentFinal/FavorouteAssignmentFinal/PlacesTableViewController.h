//
//  PlacesTableViewController.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 04/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTTPClient.h"
#import "Place.h"
#import "DetailPlaceTableViewCell.h"
#import "DetailPlaceViewController.h"

@interface PlacesTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *places;

- (void) reloadTableView;

@end
