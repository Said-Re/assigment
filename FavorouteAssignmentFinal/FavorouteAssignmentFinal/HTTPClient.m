//
//  HTTPClient.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 03/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "HTTPClient.h"

@implementation HTTPClient

+ (instancetype)sharedClient
{
    static HTTPClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[HTTPClient alloc] initWithBaseURL:[[NSURL alloc] initWithString:googlePlaceAPIBaseURLString]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self){
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        
    }
    return self;
}

- (void)getDetails:(Place *)place setDetails:(void (^)(id detailsForSend))sendData
{
    NSDictionary *parametrs = @{@"key" : googlePlaceAPIKEY,
                                @"reference": place.reference ,
                                @"sensor" : @"false"};
    
    [self GET:@"details/json" parameters:parametrs success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@", responseObject);
        NSDictionary *results = [responseObject valueForKey:@"result"];
        sendData(results);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}


- (void)getPlace:(NSString *)text setPlace:(void (^)(id placesForSend))sendData
{
    NSMutableArray *places = [[NSMutableArray alloc] init];
    
    NSDictionary *parametrs = @{@"key" : googlePlaceAPIKEY,
                                @"query": text,
                                @"sensor" : @"false"};
    
    [self GET:@"textsearch/json" parameters:parametrs success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@", responseObject);
        
        NSArray *results = [responseObject valueForKey:@"results"];
        
        for (int i = 0; i < results.count && i < 12; i++) {
            NSDictionary *dataPlace = [results objectAtIndex:i];
            Place *place = [[Place alloc] init];
            [place setData:dataPlace];
            [places addObject:place];
        }
        sendData(places);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@", error);
    }];
}

@end
