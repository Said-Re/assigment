//
//  Place.h
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 03/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Place : NSObject

@property (nonatomic, strong) NSString* reference;
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* webSite;
@property (nonatomic, strong) NSString* phone;
@property CLLocationCoordinate2D location;

- (void)setData:(NSDictionary *)json;
- (void)setDetails:(NSDictionary *)json;

@end

