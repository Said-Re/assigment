//
//  FindByTextViewController.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 04/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "FindByTextViewController.h"

@interface FindByTextViewController()

@property (weak, nonatomic) IBOutlet UITextField *text;
@property (strong, nonatomic) NSMutableArray *places;

@end

@implementation FindByTextViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.places = nil;
    self.text.delegate = self;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"findByText"]){
        PlacesTableViewController *tableController = (PlacesTableViewController *)segue.destinationViewController;
        
        [[HTTPClient sharedClient] getPlace:self.text.text setPlace:^(NSMutableArray *places){
            tableController.places = places;
            [tableController reloadTableView];
        }];
        
        
    }
}

@end
