//
//  PlacesTableViewController.m
//  FavorouteAssignmentFinal
//
//  Created by Said Rehouni on 04/10/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "PlacesTableViewController.h"

@implementation PlacesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)reloadTableView
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.places.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"detailPlaceTableViewCell";
    
    DetailPlaceTableViewCell *cell = (DetailPlaceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[DetailPlaceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Place *place = [self.places objectAtIndex:indexPath.row];
    cell.name.text = place.name;
    cell.address.text = place.address;
    cell.image.image = [self randomImage];
    
    return cell;
}

- (UIImage *)randomImage
{
    int x = arc4random() % 5;
    x++;
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%i",x]];

    return image;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"detail"]){
        DetailPlaceViewController *viewController = (DetailPlaceViewController *)segue.destinationViewController;
        Place *place = [self.places objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        
        
        [[HTTPClient sharedClient] getDetails:place setDetails:^(NSDictionary *detailsForSend) {
            [place setDetails:detailsForSend];
            viewController.place = place;
            [viewController reloadData];
        }];
         
    }
}


@end
